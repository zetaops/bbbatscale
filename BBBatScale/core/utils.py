import logging
from csv import DictReader
from hashlib import sha1
from urllib.parse import urlencode
from django.http import HttpResponse
import requests
import xmltodict

logger = logging.getLogger(__name__)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class BigBlueButton:
    def __init__(self, bbb_server_url, bbb_server_salt):
        if not bbb_server_url.startswith("https://"):
            if bbb_server_url.startswith("http://"):
                bbb_server_url = bbb_server_url[:4] + "s" + bbb_server_url[4:]
            else:
                bbb_server_url = "https://{}".format(bbb_server_url)
        if not bbb_server_url.endswith("/bigbluebutton/api/"):
            bbb_server_url = "{}/bigbluebutton/api/".format(bbb_server_url)

        self.__url = bbb_server_url
        self.__salt = bbb_server_salt

    def get_api_call_url(self, api_method, api_params):
        checksum = self.create_salt(api_method, api_params)
        if api_params:
            api_params = "{}&".format(api_params)
        return "{}{}?{}checksum={}".format(self.__url, api_method, api_params, checksum)

    def get_meetings(self):
        logger.info("Start BBB API get Meetings.")
        return requests.get(self.get_api_call_url("getMeetings", ""), timeout=1)

    @staticmethod
    def validate_get_meetings(response):
        meetings = []
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] != "SUCCESS":
            raise Exception('API Call failed')

        try:
            if data['response']['meetings'] is not None:
                if type(data['response']['meetings']['meeting']) == list:
                    meetings = data['response']['meetings']['meeting']
                else:
                    meetings.append(data['response']['meetings']['meeting'])
        except Exception as e:
            logger.error("Error while make api call getMeetings.")
            logger.error(e)
            return None

        logger.info("End BBB API get Meetings.")
        return meetings

    def create(self, params):
        api_call = self.get_api_call_url("create", self.create_params(params))
        return requests.get(api_call, timeout=5)

    @staticmethod
    def validate_create(response):
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] != "SUCCESS":
            return False
        return True

    def join(self, params):
        return self.get_api_call_url("join", self.create_params(params))

    def end(self, meeting_id, pw):
        api_call = self.get_api_call_url("end", self.create_params({"meetingID": meeting_id, "password": pw}))
        return requests.get(api_call, timeout=1)

    @staticmethod
    def validate_end(response):
        data = xmltodict.parse(response.text)
        if data['response']['returncode'] == "SUCCESS":
            return True
        return False

    def is_meeting_running(self, meeting_id):
        api_call = self.get_api_call_url("isMeetingRunning", self.create_params({"meetingID": meeting_id}))
        return requests.get(api_call, timeout=1)

    @staticmethod
    def validate_is_meeting_running(response):
        data = xmltodict.parse(response.text)
        if data['response']['running'] == "true":
            return True
        return False

    def get_meeting_infos(self, meeting_id):
        api_call = self.get_api_call_url("getMeetingInfo", self.create_params({"meetingID": meeting_id}))
        return requests.get(api_call)

    @staticmethod
    def validate_get_meeting_infos(response):
        data = xmltodict.parse(response.text)
        return data

    def create_web_hook(self, params):
        _params = self.create_params(params)
        checksum = self.create_salt("hooks/create", _params)
        url = "{}hooks/create?{}&checksum={}".format(self.__url, _params, checksum)
        return requests.get(url)

    @staticmethod
    def validate_create_web_hook(response):
        data = xmltodict.parse(response.text)
        try:
            if data['response']['returncode'] != "SUCCESS":
                return False
            return True
        except KeyError:
            logger.error("{} did not respone with a valid XML for webhook creation.")
            return False

    def create_salt(self, api_method, api_params):
        return sha1("{}{}{}".format(api_method, api_params, self.__salt).encode("utf-8")).hexdigest()

    @staticmethod
    def create_params(params):
        return urlencode(params)

    @property
    def url(self):
        return self.__url


def validate_json(json_obj: dict):  # noqa: C901 TODO
    # validates uploaded json file before importing it to db
    val_ten_name = []
    val_ins_dns = []
    val_room_name = []

    ten_dup = []
    ins_dup = []
    room_dup = []

    assert 'tenant' in json_obj
    assert 'instances' in json_obj
    assert 'rooms' in json_obj

    for tenant in json_obj["tenant"]:
        assert 'description' in tenant
        assert 'notifications_emails' in tenant
        assert 'instances' in tenant
        assert 'rooms' in tenant
        assert 'name' in tenant

        if tenant["name"] not in val_ten_name:
            val_ten_name.append(tenant["name"])
        elif tenant["name"] not in ten_dup:
            ten_dup.append(tenant["name"])

        for instance in tenant["instances"]:
            assert 'datacenter' in instance
            assert 'shared_secret' in instance
            assert 'dns' in instance

            if instance['dns'] not in val_ins_dns:
                val_ins_dns.append(instance['dns'])
            elif instance['dns'] not in ins_dup:
                ins_dup.append(instance['dns'])

        for room in tenant["rooms"]:
            assert 'is_public' in room
            assert 'comment_public' in room
            assert 'comment_private' in room
            assert 'name' in room

            if room['name'] not in val_room_name:
                val_room_name.append(room['name'])
            elif room['name'] not in room_dup:
                room_dup.append(room['name'])

    for instance in json_obj["instances"]:
        assert 'datacenter' in instance
        assert 'shared_secret' in instance
        assert 'dns' in instance

        if instance['dns'] not in val_ins_dns:
            val_ins_dns.append(instance['dns'])
        elif instance['dns'] not in ins_dup:
            ins_dup.append(instance['dns'])

    for room in json_obj["rooms"]:
        assert 'is_public' in room
        assert 'comment_public' in room
        assert 'name' in room

        if room['name'] not in val_room_name:
            val_room_name.append(room['name'])
        elif room['name'] not in room_dup:
            room_dup.append(room['name'])

    return [set(ten_dup), set(ins_dup), set(room_dup)]


def validate_csv(reader: DictReader):
    # validates uploaded csv file before importing it to db
    val_room_name = []

    room_dup = []

    for row in reader:
        assert 'name' in row
        assert 'is_public' in row
        assert 'comment_public' in row
        assert 'comment_private' in row

        if row['name'] not in val_room_name:
            val_room_name.append(row['name'])
        elif row['name'] not in room_dup:
            room_dup.append(row['name'])

    return [set(room_dup)]


def validate_bbb_api_call(api_method, params, tenant):
    checksum = params.pop('checksum')
    _checksum = sha1(
        "{}{}{}".format(api_method, BigBlueButton.create_params(params), tenant.token_registration).encode(
            "utf-8")).hexdigest()
    return True if checksum[0] == _checksum else False


def bbb_checksum_error_xml_response():
    return HttpResponse("""
        <response>
            <returncode>FAILED</returncode>
            <messageKey>checksumError</messageKey>
            <message>You did not pass the checksum security check</message>
        </response>
    """, content_type='text/xml')


def bbb_meeting_not_found():
    return HttpResponse("""
        <response>
            <returncode>FAILED</returncode>
            <messageKey>notFound</messageKey>
            <message>We could not find a meeting with that meeting ID</message>
        </response>
    """, content_type='text/xml')
