from django.test import TestCase
from freezegun import freeze_time
import os
from core.models import Room, Instance, Tenant, RoomEvent
from core.management.commands import collect_all_room_occupancy


class CollectRoomOccupancy(TestCase):

    def setUp(self):
        self.fbi = Tenant.objects.create(
            name="FBI",
        )

        self.bbb_instance = Instance.objects.create(
            tenant=self.fbi,
            dns="bbb10.fbi.h-da.de",
            participant_count_max=10,
            videostream_count_max=2,
        )

        self.rooms = []

        file_path = "file://" + os.getcwd() + "/core/tests/test_data/SimpleICalCollector_testdata.ics"
        self.rooms.append(Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D14/02.04",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="SimpleICalCollector",
            event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
        ))
        self.rooms.append(Room.objects.create(
            tenant=self.fbi,
            instance=self.bbb_instance,
            name="D14/02.05",
            participant_count=10,
            videostream_count=5,
            event_collection_strategy="SimpleICalCollector",
            event_collection_parameters='{"iCal_url": "' + file_path + '", "iCal_encoding": "UTF-8"}',
        ))

    @freeze_time("2020-05-25 06:30:00", tz_offset=0)
    def test_command_real_data(self):
        collect_all_room_occupancy.Command.handle(self)

        room_events = RoomEvent.objects.all()
        self.assertEqual(len(room_events), 4)
